<?php


class Routing{


    public static function buildRoute(){
        $controllerName = "IndexController";
        $action = "index";

        $urlPathWithoutParameters = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);

        $route = explode("/", $urlPathWithoutParameters);
        if($route[1]!='' && $route[1]!= 'index.php'){
            $controllerName = ucfirst($route[1]."Controller");
        }

        include CONTROLLER_PATH.$controllerName.".php";

        if(isset($route[2]) && $route[2]!=''){
            $action = $route[2];
        }
        $controller = new $controllerName();
        $controller->$action();
    }
}