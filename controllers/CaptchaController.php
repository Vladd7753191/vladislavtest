<?php

use Gregwar\Captcha\CaptchaBuilder;

class CaptchaController extends Controller
{
    public function index()
    {
        $builder = new CaptchaBuilder();
        $builder->build();

        $_SESSION['captchaCode'] = $builder->getPhrase();

        header('Content-type: image/jpeg');
        $builder->output();
    }

    public function native()
    {
        $captchaCode = $this->randomString(6);

        $_SESSION['captchaCode'] = $captchaCode;

        $height = 60;
        $width = 170;

        $image = imagecreatetruecolor($width, $height);

        $black = imagecolorallocate($image, 0, 0, 0);
        $white = imagecolorallocate($image, 255, 220, 90);

        imagefilledrectangle($image, 0, 0, $width, $height, $white);

        //TODO сделать красивый вывод через imagettftext
        imagestring($image, 5, 56, 20, $captchaCode, $black);

        header("Content-type: image/png");
        imagepng($image);
    }

    //TODO move from controller
    private function randomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < $length; $i++) {
            $randstring.= $characters[rand(0, strlen($characters))];
        }
        return $randstring;
    }
}