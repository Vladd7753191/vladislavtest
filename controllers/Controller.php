<?php

abstract class Controller
{
    public $model;
    public $view;
    protected $pageData = [];

    public function __construct()
    {
        $this->view = new Views();
    }

    public function getParameter(string $key, $defaultValue = null)
    {
        return $_GET[$key] ?? $defaultValue;
    }

    public function getPostParameter(string $key, $defaultValue = null)
    {
        return $_POST[$key] ?? $defaultValue;
    }

    public function checkAuth()
    {
        $user = $this->getUser();

        if (null === $user) {
            header("Location:/user/loginForm");
        }
    }

    public function checkApiAuth()
    {
        $user = $this->getApiUser();

        if (!$user) {
            //TODO throw ApiAccessDeniedException
         //   header("Location:/user/registerForm");

            // send header with 401 status response. Unauthorised
      // echo json_encode(['error'=> $exception->getMessage()]);

            throw new Exception('Access  denied');
        }
    }

    public function getApiUser()
    {
        $apiKey = $this->getParameter('apiKey');
        $sql = "SELECT * FROM users WHERE apikey = :apiKey";
        $stmt = DB::connToDB()->prepare($sql);
        $stmt->bindValue("apiKey", $apiKey, PDO::PARAM_STR);
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        return $user;
    }

    public function getUser()
    {
        return $_SESSION["userId"] ?? null;
    }

    public function isAjax()
    {
        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }
}