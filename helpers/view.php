<?php

function sortUrl($fieldName)
{
    $sortOrder = (isset($_GET['sortOrder']) && $_GET['sortOrder'] === 'ASC') ? 'DESC' : 'ASC';

    return "/?sortField=$fieldName&sortOrder=$sortOrder";
}

function sortArrowSign($fieldName) {
    $sortableField = $_GET['sortField'] ?? null;

    if ($sortableField === null) return '';

    if ($sortableField !== $fieldName) return '';

    $sortOrder = $_GET['sortOrder'] ?? null;

    return $sortOrder === 'DESC' ? '↑' : '↓';
}