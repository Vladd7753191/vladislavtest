<?php

require_once VALIDATOR_PATH.'ValidationException.php';

class RegisterValidator
{
    public $formData = [];

    public $errors = [];

    public $fields = [
        'name'     => 'validateName',
        'email'    => 'validateEmail',
        'login'    => 'validateLogin',
        'password' => 'validatePassword',
        'captcha'  => 'validateCaptcha',
    ];

    public function __construct($formData = [])
    {
        $this->formData = $formData;
    }

    public function validate()
    {
        foreach ($this->fields as $field => $validateMethod) {
            try {
                $this->$validateMethod($this->formData[$field]);
            } catch (ValidationException $validationException) {
                $this->errors[] = $validationException->getMessage();
            } catch (\Throwable $exception) {
                throw $exception;
            }
        }

        return empty($this->errors);
    }

    private function validateEmail(string $email)
    {
        if (empty($email)) {
            throw new ValidationException('Email пустой');
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new ValidationException('Email имеет неверный формат');
        }

        if ($this->quantEmailTable($email) > 0) {
            throw new ValidationException('Email не уникален.');
        }
    }


    private function validateLogin($login)
    {
        if (empty($name)) {
            throw new ValidationException('Логин не может быть пустым');
        }

        if (!preg_match("^(?=.{3,24})[a-z][a-z0-9]*[._-]?[a-z0-9]+$", $login)) {
            throw new ValidationException('Логин имеет неверный формат');
        }
        return true;
    } // validateLogin


    private function validatePassword($password)
    {
        if (empty($password)) {
            throw new ValidationException('Пароль не может быть пустым');
        }

        if (!preg_match("^(?=.{3,24})[a-z][a-z0-9]*[._-]?[a-z0-9]+$", $password)) {
            throw new ValidationException('Пароль имеет неверный формат');
        }
        return true;
    }

    private function validateName(string $name)
    {
        if (empty($name)) {
            throw new ValidationException('Имя не может быть пустым');
        }
        if (!preg_match("/^[a-zA-Zа-яёА-ЯЁ\s\-]+$/u", $name)) {
            throw new ValidationException('Имя имеет неверный формат');
        }
        return true;
    }

    private function quantEmailTable(string $email): int
    {
        $sql = "SELECT * FROM users WHERE email = :email";
        $stmt = DB::connToDB()->prepare($sql);
        $stmt->bindValue("email", $email, PDO::PARAM_STR);
        $stmt->execute();
        return $res = $stmt->rowCount();
    }

    private function validateCaptcha(string $captcha)
    {
        if (empty($captcha)) {
            throw new ValidationException('Проверочный код пустой');
        }

        if (strtolower($captcha) !== strtolower($_SESSION['captchaCode'])) {
            throw new ValidationException('Вы робот!');
        }
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}